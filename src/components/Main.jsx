import React, {useState, useEffect} from "react";

export default function Main() {
    const [meme, setMeme] = useState({
        topText: "",
        bottomText: "",
        randomImage: "http://i.imgflip.com/1bij.jpg"
    })

    const [allMemes, setAllMemes] = useState([])

    useEffect(() => {
        fetch("https://api.imgflip.com/get_memes")
            .then(res => res.json())
            .then(data => setAllMemes(data.data.memes))
    }, [])

    function getMemeImage() {
        const randomNumber = Math.floor(Math.random() * allMemes.length)
        const url = allMemes[randomNumber].url
        setMeme((prevMeme) => ({
            ...prevMeme,
            randomImage: url
        }))
    }

    function handleChange(event) {
        const {name, value} = event.target
        setMeme(prevMeme => ({
            ...prevMeme,
            [name]: value
        }))
    }

    return (
        <main>
            <div className="row justify-content-md-center">
                <div className="col-10 main-buttons">
                    <div className="d-grid gap-5">
                            <div className="row justify-content-center">
                                <div className="row justify-content-evenly">
                                    <div className="col">
                                        <input type="text"
                                               className="form-control form"
                                               placeholder="Top text"
                                               name="topText"
                                               value={meme.topText}
                                               onChange={handleChange}
                                        />
                                    </div>
                                    <div className="col">
                                        <input type="text"
                                               className="form-control form"
                                               placeholder="Bottom text"
                                               name="bottomText"
                                               value={meme.bottomText}
                                               onChange={handleChange}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row justify-content-center">
                                <div className="row justify-content-evenly ">
                                    <button onClick={getMemeImage} className="btn main-btn" type="button">Get new meme image</button>
                                </div>
                            </div>
                    </div>
                    <div className="container text-center">
                        <div className="row justify-content-md-center">
                            <div className="col-md-auto meme">
                                <img src={meme.randomImage} className="img-fluid" alt="..." />
                                <h2 className="meme--text top">{meme.topText}</h2>
                                <h2 className="meme--text bottom">{meme.bottomText}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    )
}