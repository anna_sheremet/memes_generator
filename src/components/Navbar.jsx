import React from "react";
import troll from "../assets/troll_face.svg"
export default function Navbar() {
    return (
        <nav className="navbar nav-style nav">
            <div className="container-fluid">
                <a className="navbar-brand nav-logo">
                    <img src={troll} alt="Logo" width="30" height="24" className="d-inline-block align-text-top" />
                        Memes & You
                </a>
            </div>
        </nav>
    )
}